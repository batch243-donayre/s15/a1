// console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	// console.log("My full name is" + fullName);
		fullName = "John";
		console.log("First Name : " + fullName)

	lastName = "Smith";
	console.log("First Name : " + lastName)

	let age = 40;
	age	= 30;
	console.log("Age: " + age);

	let hobbiesTitle = "Hobbies : ";
	console.log(hobbiesTitle);

	let hobbies = ["Biking","Mountain Climbing","Swimming"];
	console.log(hobbies);

	let workAddress = "Working Address : ";
	console.log(workAddress);

	let workingAddress = {
		houseNumber : 32,
		street : "Washington",
		city : "Lincoln",
		store: "Nebraska"
	}
	console.log(workingAddress);

	fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);
	age = 40;
	console.log("My current age is: " + age);


	let friendTitle = " My Friends are : ";
	console.log(friendTitle);

	let friends = ["Tony","Bruce","Thor", "Natasha ","Clint","Nick"];
	console.log(friends);

	let profile = {

		username: "captain/_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ");
	console.log(profile);

	fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	const lastLocation = "Arctic Ocean";
	// lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);
